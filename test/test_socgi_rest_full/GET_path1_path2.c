#include "test_socgi_rest_full.h"

int GET_path1_path2( struct RestServiceContext *ctx )
{
	char		response[4096] ;
	int		response_len ;
	
	int		uri_paths_count ;
	char		*uri_path = NULL ;
	int		uri_path_len ;
	
	int		queries_count ;
	
	int		nret = 0 ;
	
	/* 初始化响应报文 */
	memset( response , 0x00 , sizeof(response) );
	response_len = 0 ;
	
	/* 公共检查 */
	TravelUriPathsAndQueries( ctx , response , sizeof(response) , & response_len );
	
	/* 私有检查 */
	BUFSTRCAT( response , response_len , "--- REST PRICATE CHECK ---\n" )
	
	/* 检查路径数 */
	uri_paths_count = RESTGetHttpUriPathsCount( ctx ) ;
	if( uri_paths_count == 2 )
	{
		BUFPRINTF( response , response_len , "check uri_paths_count[%d] ok\n" , uri_paths_count )
	}
	else
	{
		BUFPRINTF( response , response_len , "check uri_paths_count[%d] failed\n" , uri_paths_count )
		BUFSTRCAT( response , response_len , "--------------------------- CHECK RESULT : FAILURE ---\n" )
		goto _GOTO_GO_RESPONSE;
	}
	
	/* 检查路径值 */
	uri_path = RESTGetHttpUriPathPtr( ctx , 1 , & uri_path_len ) ;
	if( STRNEQRSTR( uri_path , uri_path_len , "path1" ) )
	{
		BUFSTRCAT( response , response_len , "check uri_path ok\n" )
	}
	else
	{
		BUFSTRCAT( response , response_len , "check uri_path failed\n" )
		BUFSTRCAT( response , response_len , "--------------------------- CHECK RESULT : FAILURE ---\n" )
		goto _GOTO_GO_RESPONSE;
	}
	
	uri_path = RESTGetHttpUriPathPtr( ctx , 2 , & uri_path_len ) ;
	if( STRNEQRSTR( uri_path , uri_path_len , "path2" ) )
	{
		BUFSTRCAT( response , response_len , "check uri_path ok\n" )
	}
	else
	{
		BUFSTRCAT( response , response_len , "check uri_path failed\n" )
		BUFSTRCAT( response , response_len , "--------------------------- CHECK RESULT : FAILURE ---\n" )
		goto _GOTO_GO_RESPONSE;
	}
	
	/* 检查参数数 */
	queries_count = RESTGetHttpUriQueriesCount( ctx ) ;
	if( queries_count == 0 )
	{
		BUFPRINTF( response , response_len , "check queries_count[%d] ok\n" , queries_count )
	}
	else
	{
		BUFPRINTF( response , response_len , "check queries_count[%d] failed\n" , queries_count )
		BUFSTRCAT( response , response_len , "--------------------------- CHECK RESULT : FAILURE ---\n" )
		goto _GOTO_GO_RESPONSE;
	}
	
	/* 测试结论 */
	BUFSTRCAT( response , response_len , "--------------------------- CHECK RESULT : SUCCESS ---\n" )
	
_GOTO_GO_RESPONSE :
	
	/* 设置响应报文 */
	nret = RESTFormatHttpResponse( ctx , response , response_len , NULL ) ;
	if( nret )
	{
		ErrorLog( __FILE__ , __LINE__ , "RESTFormatHttpResponse failed[%d] , errno[%d]" , nret , errno );
		return nret;
	}
	else
	{
		InfoLog( __FILE__ , __LINE__ , "RESTFormatHttpResponse ok" );
	}
	
	return 0;
}

