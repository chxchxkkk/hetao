/*
 * hetao - High Performance Web Server
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

#include "hetao_in.h"

int CreateAllListenSession( struct HetaoEnv *p_env , hetao_conf *p_conf , struct NetAddress *old_netaddr_array , int old_netaddr_array_count )
{
	int			l , w ;
	
	struct ListenSession	*p_listen_session = NULL ;
	struct NetAddress	*p_netaddr = NULL ;
	
	struct VirtualHost	*p_virtual_host = NULL ;
	
	int			nret = 0 ;
	
	for( l = 0 ; l < p_conf->_listen_count ; l++ )
	{
		p_listen_session = (struct ListenSession *)malloc( sizeof(struct ListenSession) ) ;
		if( p_listen_session == NULL )
		{
			ErrorLog( __FILE__ , __LINE__ , "malloc failed , errno[%d]" , ERRNO );
			return -1;
		}
		memset( p_listen_session , 0x00 , sizeof(struct ListenSession) );
		
		p_listen_session->type = DATASESSION_TYPE_LISTEN ;
		
		LIST_ADD( & (p_listen_session->list) , & (p_env->listen_session_list.list) );
		
		p_netaddr = GetListener( old_netaddr_array , old_netaddr_array_count , p_conf->listen[l].ip , p_conf->listen[l].port ) ;
		if( p_netaddr )
		{
			/* 上一辈侦听信息中有本次侦听相同地址 */
			memcpy( & (p_listen_session->netaddr) , p_netaddr , sizeof(struct NetAddress) );
			DebugLog( __FILE__ , __LINE__ , "[%s:%d] reuse #%d#" , p_conf->listen[l].ip , p_conf->listen[l].port , p_listen_session->netaddr.sock );
			p_env->listen_session_count++;
		}
		else
		{
			/* 上一辈侦听信息中没有本次侦听相同地址，老老实实创建新的侦听端口 */
#if ( defined __linux ) || ( defined __unix )
			p_listen_session->netaddr.sock = socket( AF_INET , SOCK_STREAM , IPPROTO_TCP ) ;
			if( p_listen_session->netaddr.sock == -1 )
			{
				ErrorLog( __FILE__ , __LINE__ , "socket failed , errno[%d]" , ERRNO );
				return -1;
			}
#elif ( defined _WIN32 )
			p_listen_session->netaddr.sock = WSASocket( AF_INET , SOCK_STREAM , 0 , NULL , 0 , WSA_FLAG_OVERLAPPED ) ;
			if( p_listen_session->netaddr.sock == -1 )
			{
				ErrorLog( __FILE__ , __LINE__ , "socket failed , errno[%d]" , ERRNO );
				return -1;
			}
#endif
			
#if ( defined __linux ) || ( defined __unix )
			SetHttpNonblock( p_listen_session->netaddr.sock );
#endif
			SetHttpReuseAddr( p_listen_session->netaddr.sock );
			
			strncpy( p_listen_session->netaddr.ip , p_conf->listen[l].ip , sizeof(p_listen_session->netaddr.ip)-1 );
			p_listen_session->netaddr.port = p_conf->listen[l].port ;
			SETNETADDRESS( p_listen_session->netaddr )
			nret = bind( p_listen_session->netaddr.sock , (struct sockaddr *) & (p_listen_session->netaddr.addr) , sizeof(struct sockaddr) ) ;
			if( nret == -1 )
			{
				ErrorLog( __FILE__ , __LINE__ , "bind[%s:%d] failed , errno[%d]" , p_listen_session->netaddr.ip , p_listen_session->netaddr.port , ERRNO );
				return -1;
			}
			
			nret = listen( p_listen_session->netaddr.sock , 10240 ) ;
			if( nret == -1 )
			{
				ErrorLog( __FILE__ , __LINE__ , "listen failed , errno[%d]" , ERRNO );
				return -1;
			}
			
			DebugLog( __FILE__ , __LINE__ , "[%s:%d] listen #%d#" , p_listen_session->netaddr.ip , p_conf->listen[l].port , p_listen_session->netaddr.sock );
			p_env->listen_session_count++;
		}
		
		/* 创建SSL环境 */
		if( p_conf->listen[l].ssl.certificate_file[0] )
		{
			if( p_env->init_ssl_env_flag == 0 )
			{
				SSL_library_init();
				OpenSSL_add_ssl_algorithms();
				SSL_load_error_strings();
				p_env->init_ssl_env_flag = 1 ;
			}
			
			p_listen_session->ssl_ctx = SSL_CTX_new( SSLv23_method() ) ;
			if( p_listen_session->ssl_ctx == NULL )
			{
				ErrorLog( __FILE__ , __LINE__ , "SSL_CTX_new failed , errno[%d]" , ERRNO );
				return -1;
			}
			
			nret = SSL_CTX_use_certificate_file( p_listen_session->ssl_ctx , p_conf->listen[l].ssl.certificate_file , SSL_FILETYPE_PEM ) ;
			if( nret <= 0 )
			{
				ErrorLog( __FILE__ , __LINE__ , "SSL_CTX_use_certificate_file failed , errno[%d]" , ERRNO );
				return -1;
			}
			else
			{
				DebugLog( __FILE__ , __LINE__ , "SSL_CTX_use_certificate_file[%s] ok" , p_conf->listen[l].ssl.certificate_file );
			}
			
			nret = SSL_CTX_use_PrivateKey_file( p_listen_session->ssl_ctx , p_conf->listen[l].ssl.certificate_key_file , SSL_FILETYPE_PEM ) ;
			if( nret <= 0 )
			{
				ErrorLog( __FILE__ , __LINE__ , "SSL_CTX_use_PrivateKey_file failed , errno[%d]" , ERRNO );
				return -1;
			}
			else
			{
				DebugLog( __FILE__ , __LINE__ , "SSL_CTX_use_PrivateKey_file[%s] ok" , p_conf->listen[l].ssl.certificate_key_file );
			}
		}
		
		/* 注册所有虚拟主机 */
		nret = InitVirtualHostHash( & (p_listen_session->virtual_hosts) , p_conf->listen[l]._website_count ) ;
		if( nret )
		{
			ErrorLog( __FILE__ , __LINE__ , "InitVirtualHostHash failed[%d]" , nret );
			return -1;
		}
		
		for( w = 0 ; w < p_conf->listen[l]._website_count ; w++ )
		{
			if( p_conf->listen[l].website[w].wwwroot[0] == '\0' )
				continue;
			
			/* 创建虚拟主机 */
			p_virtual_host = CreateVirtualHostHashNode( p_env , p_conf , l , w , p_conf->listen[l].website[w].domain , p_conf->listen[l].website[w].wwwroot , p_conf->listen[l].website[w].index , p_conf->listen[l].website[w].access_log ) ;
			if( p_virtual_host == NULL )
			{
				ErrorLog( __FILE__ , __LINE__ , "CreateVirtualHostHashNode failed" );
				return -1;
			}
			else
			{
				InfoLog( __FILE__ , __LINE__ , "CreateVirtualHostHashNode ok" );
			}
			
			/* 增加虚拟主机 */
			nret = PushVirtualHostHashNode( & (p_listen_session->virtual_hosts) , p_virtual_host ) ;
			if( nret )
			{
				ErrorLog( __FILE__ , __LINE__ , "PushVirtualHostHashNode[%s][%s] failed[%d] , errno[%d]" , p_virtual_host->domain , p_virtual_host->wwwroot , nret , ERRNO );
				return -1;
			}
			else
			{
				DebugLog( __FILE__ , __LINE__ , "add virtual host[%s] wwwroot[%s]" , p_virtual_host->domain , p_virtual_host->wwwroot , nret );
			}
			
			if( p_virtual_host->domain[0] == '\0' && p_listen_session->virtual_hosts.p_virtual_host_default == NULL )
				p_listen_session->virtual_hosts.p_virtual_host_default = p_virtual_host ;
		}
	}
	
	return 0;
}

