/*
 * hetao - High Performance Web Server
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

#include "hetao_in.h"

#include "hetao_socgi.h"

void SOCGISetUserData( struct HttpApplicationContext *ctx , void *user_data )
{
	ctx->user_data = user_data ;
	return;
}

void *SOCGIGetUserData( struct HttpApplicationContext *ctx )
{
	return ctx->user_data;
}

char *SOCGIGetCurrentPathname( struct HttpApplicationContext *ctx )
{
	return ctx->p_hetao_env->current_pathname;
}

char *SOCGIGetConfigPathfilename( struct HttpApplicationContext *ctx )
{
	return ctx->p_uri_location->socgi.socgi_config_pathfilename;
}

char *SOCGIGetHttpHeaderPtr_METHOD( struct HttpApplicationContext *ctx , int *p_value_len )
{
	return GetHttpHeaderPtr_METHOD( ctx->p_http_session->http , p_value_len );
}

char *SOCGIGetHttpHeaderPtr_URI( struct HttpApplicationContext *ctx , int *p_value_len )
{
	return GetHttpHeaderPtr_URI( ctx->p_http_session->http , p_value_len );
}

char *SOCGIGetHttpHeaderPtr_VERSION( struct HttpApplicationContext *ctx , int *p_value_len )
{
	return GetHttpHeaderPtr_VERSION( ctx->p_http_session->http , p_value_len );
}

char *SOCGIQueryHttpHeaderPtr( struct HttpApplicationContext *ctx , char *name , int *p_value_len )
{
	return QueryHttpHeaderPtr( ctx->p_http_session->http , name , p_value_len );
}

struct HttpHeader *SOCGITravelHttpHeaderPtr( struct HttpApplicationContext *ctx , struct HttpHeader *p_header )
{
	return TravelHttpHeaderPtr( ctx->p_http_session->http , p_header );
}

char *SOCGIGetHttpHeaderNamePtr( struct HttpHeader *p_header , int *p_name_len )
{
	return GetHttpHeaderNamePtr( p_header , p_name_len );
}

char *SOCGIGetHttpHeaderValuePtr( struct HttpHeader *p_header , int *p_value_len )
{
	return GetHttpHeaderValuePtr( p_header , p_value_len );
}

char *SOCGIGetHttpBodyPtr( struct HttpApplicationContext *ctx , int *p_body_len )
{
	return GetHttpBodyPtr( ctx->p_http_session->http , p_body_len );
}

int SOCGIFormatHttpResponseV( struct HttpEnv *http , char *http_response_body , int http_response_body_len , char *http_header_format , va_list valist )
{
	struct HttpBuffer	*b = NULL ;
	
	int			nret = 0 ;
	
	nret = FormatHttpResponseStartLine( HTTP_OK , http , 0 , NULL ) ;
	if( nret )
		return SOCGI_FATAL_FASTERHTTP;
	
	b = GetHttpResponseBuffer(http) ;
	
	if( http_header_format )
	{
		nret = StrcatvHttpBuffer( b , http_header_format , valist ) ;
		if( nret )
			return SOCGI_FATAL_FASTERHTTP;
	}
	
	if( http_response_body == NULL )
	{
		nret = StrcatHttpBuffer( b , HTTP_RETURN_NEWLINE ) ;
		if( nret )
			return SOCGI_FATAL_FASTERHTTP;
	}
	else
	{
		nret = StrcatfHttpBuffer( b , "Content-length: %d" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE , http_response_body_len ) ;
		if( nret )
			return SOCGI_FATAL_FASTERHTTP;
		
		nret = MemcatHttpBuffer( b , http_response_body , http_response_body_len ) ;
		if( nret )
			return SOCGI_FATAL_FASTERHTTP;
	}
	
	return 0;
}

int SOCGIFormatHttpResponse( struct HttpApplicationContext *ctx , char *http_response_body , int http_response_body_len , char *http_header_format , ... )
{
	va_list			valist ;
	
	int			nret = 0 ;
	
	va_start( valist , http_header_format );
	nret = SOCGIFormatHttpResponseV( ctx->p_http_session->http , http_response_body , http_response_body_len , http_header_format , valist ) ;
	va_end( valist );
	
	return nret;
}

