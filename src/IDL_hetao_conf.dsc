STRUCT	hetao_conf
{
	INT	4	worker_processes
	INT	4	cpu_affinity
	INT	4	accept_mutex
	
	STRING	256	error_log
	STRING	6	log_level
	
	STRING	64	user
	
	STRUCT	limits
	{
		INT	4	max_http_session_count
		INT	4	max_file_cache
		INT	4	max_connections_per_ip
		INT	4	max_headers_count
		INT	4	max_headers_length
		INT	4	max_header_content_length
	}
	
	STRUCT	listen	ARRAY	8
	{
		STRING	15	ip
		INT	4	port
		
		STRUCT	ssl
		{
			STRING	256	certificate_file
			STRING	256	certificate_key_file
		}
		
		STRUCT	website	ARRAY	16
		{
			STRING	256	domain
			STRING	256	wwwroot
			STRING	64	index
			STRING	256	access_log
			
			STRUCT	redirect ARRAY	4
			{
				STRING	64	domain
				STRING	64	new_domain
			}
			
			STRUCT	rewrite	ARRAY	16
			{
				STRING	256	pattern
				STRING	256	new_uri
			}
			
			STRUCT	forward
			{
				STRING	16	forward_type
				STRING	1	forward_rule
				
				STRUCT	ssl
				{
					STRING	256	certificate_file
					STRING	256	certificate_key_file
				}
				
				STRUCT	forward_server	ARRAY	100
				{
					STRING	15	ip
					INT	4	port
				}
			}
			
			STRUCT	socgi
			{
				STRING	16	socgi_type
				STRING	256	socgi_config_pathfilename
				STRING	256	socgi_bin_pathfilename
			}
			
			STRUCT	location	ARRAY	16
			{
				STRING	256	location
				
				STRUCT	redirect ARRAY	16
				{
					STRING	64	domain
					STRING	64	new_domain
				}
				
				STRUCT	rewrite	ARRAY	4
				{
					STRING	256	pattern
					STRING	256	new_uri
				}
				
				STRUCT	forward
				{
					STRING	16	forward_type
					STRING	1	forward_rule
					
					STRUCT	ssl
					{
						STRING	256	certificate_file
						STRING	256	certificate_key_file
					}
					
					STRUCT	forward_server	ARRAY	100
					{
						STRING	15	ip
						INT	4	port
					}
				}
				
				STRUCT	socgi
				{
					STRING	16	socgi_type
					STRING	256	socgi_config_pathfilename
					STRING	256	socgi_bin_pathfilename
				}
			}
		}
	}
	
	STRUCT	tcp_options
	{
		INT	4	nodelay
		INT	4	linger
	}
	
	STRUCT	http_options
	{
		INT	4	compress_on
		INT	4	timeout
		INT	4	elapse
		INT	4	forward_disable
	}
	
	STRUCT	error_pages
	{
		STRING	256	error_page_400
		STRING	256	error_page_401
		STRING	256	error_page_403
		STRING	256	error_page_404
		STRING	256	error_page_408
		STRING	256	error_page_500
		STRING	256	error_page_503
		STRING	256	error_page_505
	}
	
	STRUCT	mime_types
	{
		STRUCT	mime_type	ARRAY	256
		{
			STRING	50	type
			STRING	100	mime
			INT	1	compress_enable
		}
	}
}

